import {Graphics} from 'pixi.js';

export type Cells = Array<Array<Cell>>;

export enum CellState {
    Dead,
    Alive
}

export class Cell {
    state: CellState;
    constructor(state?: CellState) {
        this.state = state ? state : CellState.Dead;
    }
}

export const createNewCells = (cellAmount: number) : Cells => {
    let cells : Cells = Array(cellAmount);

    for (let i = 0; i < cellAmount; i++) {
        let row = Array(cellAmount);
        for (let j = 0; j < cellAmount; j++) {
            row[j] = new Cell();
        }
        cells[i] = row;
    }
    return cells;
};

export const drawGrid = (totalSideLength: number, cells: Array<Array<Cell>>, g: Graphics) => {
    const gridSideLength = Math.floor(totalSideLength / cells.length);
    g.clear();
    cells.forEach((row, i) => {
        row.forEach((cell, j) => {
            g.beginFill((cell.state == CellState.Alive) ? 0x00ccff : 0xff004d);
            g.drawRect(j*gridSideLength, i*gridSideLength, gridSideLength, gridSideLength);
            g.endFill();
        });
    });
};

export const getCell = (cells: Array<Array<Cell>>, row: number, col: number) : Cell => {
    // console.log(row, col);
    row = (row < 0) ? (cells.length)+row : row;
    col = (col < (0)) ? (cells[0].length)+col : col;
    row = (row > (cells.length-1)) ? row-(cells.length) : row;
    col = (col > (cells[0].length-1)) ? col-(cells[0].length) : col;
    // console.log(row, col);
    return cells[row][col];
};

export const getNeighbors = (cells: Array<Array<Cell>>, row: number, col: number) : Array<Cell> => {
    let result : Array<Cell> = [];
    const srow = row-1;
    const scol = col-1;
    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
            result.push(getCell(cells, i+srow, j+scol));
        }
    }
    result.splice(4, 1);
    return result;
};

export const updateCellByXY = (cells: Array<Array<Cell>>, x: number, y: number, totalSideLength: number) => {
    const row = Math.floor(y / totalSideLength * cells.length);
    const col = Math.floor(x / totalSideLength * cells.length);
    // console.log(y)
    // console.log(row);
    const cell = getCell(cells, row, col);
    cell.state = (cell.state === CellState.Alive) ? CellState.Dead : CellState.Alive;
};

export const updateCells = (cells: Array<Array<Cell>>) : Array<Array<Cell>> => {
    const newCells : Array<Array<Cell>> = JSON.parse(JSON.stringify(cells));
    for (let i = 0; i < cells.length; i++) {
        for (let j = 0; j < cells[0].length; j++) {
            let liveNeighbors = 0;
            getNeighbors(cells, i, j).forEach((cell) => {
                if (cell.state == CellState.Alive) liveNeighbors++;
            });
            const currentCell = newCells[i][j];
            if (currentCell.state == CellState.Alive) {
                switch (true) {
                    case (liveNeighbors < 2):
                        currentCell.state = CellState.Dead;
                    case (liveNeighbors > 3):
                        currentCell.state = CellState.Dead;
                }
            } else {
                if (liveNeighbors === 3) {
                    currentCell.state = CellState.Alive;
                }
            }
            // if (i == 51) console.log(`${i} ${j} ${JSON.stringify(getNeighbors(cells, i, j))}  ${currentCell.state}`);
        }
    }
    return newCells;
};





