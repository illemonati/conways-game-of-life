import * as PIXI from 'pixi.js';

import {Cell, CellState, drawGrid, getCell, updateCellByXY, updateCells, Cells, createNewCells} from './cellutils';
import * as presetsutils from './presetsutils'
import {loadPresets} from "./presetsutils";

let width = 800;
let height = 800;

let app = new PIXI.Application({
    autoResize: true,
    width: width,
    height: height,
    // resolution: devicePixelRatio
});
let canvas = app.view;


document.querySelector("#canvasDiv")?.appendChild(canvas);

const g = new PIXI.Graphics();
g.position.set(0, 0)
app.stage.addChild(g);
app.renderer.backgroundColor = 0xffffff;


const cellAmount = 80;
let cells = createNewCells(cellAmount);




const changeCellsOption = (select: HTMLSelectElement) => {
    const index = (select.selectedIndex == -1) ? 0 : select.selectedIndex;
    const save : presetsutils.PresetGrid = JSON.parse(select.options[index].getAttribute('data-save')!);
    cells = presetsutils.parseSaveToCells(save, cellAmount);
    drawGrid(width, cells, g);
};

const presetElement = document.querySelector("#presets")! as HTMLSelectElement;

presetsutils.loadPresets(presetElement).then(() => {
    changeCellsOption(presetElement! as HTMLSelectElement);
});

console.log(cells);

const timeBetweenUpdates = 2000;

const mainLoop = () => {
    cells = updateCells(cells);
    drawGrid(width, cells, g);
};


drawGrid(width, cells, g);


presetElement.addEventListener('change', (evt) => {
    const select : HTMLSelectElement = evt.target as HTMLSelectElement;
    changeCellsOption(select);
});




document.querySelector("#savePreset")?.addEventListener('click', (evt) => {
    const input = document.querySelector("#presetNameInput") as HTMLInputElement;
    const name = input.value;
    presetsutils.saveCustomPreset(cells, name).then(() => {
        loadPresets(presetElement).then();
    });
});


document.querySelector("#stepButton")?.addEventListener('click', mainLoop);
const startButton = document.querySelector("#startButton")!;
const secsInput: HTMLInputElement = document.querySelector('#secsInput')! as HTMLInputElement;
startButton.addEventListener('click', () => {
    if (startButton.textContent === 'start') {
        mainLoop();
        const secs = parseFloat(secsInput.value) * 1000;
        const intervalID = setInterval(mainLoop, secs);
        startButton.textContent = 'stop';
        startButton.setAttribute('data-intervalID', JSON.stringify(intervalID));
    } else {
        const intervalID: number = JSON.parse(startButton.getAttribute('data-intervalID')!);
        startButton.removeAttribute('data-intervalID');
        clearInterval(intervalID);
        startButton.textContent = 'start';
    }
})

const clickHandler = (event: any) => {
    updateCellByXY(cells, event.data.global.x, event.data.global.y, width);
    drawGrid(width, cells, g);
};

app.renderer.plugins.interaction.on('mousedown', clickHandler);
app.renderer.plugins.interaction.on('touchstart', clickHandler);



