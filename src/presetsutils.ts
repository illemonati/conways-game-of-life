import {Cells, CellState, createNewCells, getCell} from "./cellutils";

export type PresetGrid = Array<Array<number>>;

interface Preset {
    name: string,
    shape: PresetGrid
}

interface PresetGroup{
    name: string,
    presets: Array<Preset>
}

interface PresetsFile {
    presetGroups : Array<PresetGroup>
}

const loadPresetsFile = async (): Promise<PresetsFile> => {
    const localStoragePresets = localStorage.getItem('presets');
    return (!!localStoragePresets) ? JSON.parse(localStoragePresets) : await fetchDefaultPresets();
};

export const loadPresets = async (presetsElement: HTMLSelectElement) => {
    const presetsFile = await loadPresetsFile();
    savePresetsFile(presetsFile).then();
    const presetGroups = presetsFile.presetGroups;
    const newPresetsElement: HTMLSelectElement = document.createElement('select');
    presetGroups.forEach((presetGroup) => {
        const groupElement: HTMLOptGroupElement = document.createElement("optgroup");
        groupElement.label = presetGroup.name;
        newPresetsElement.add(groupElement);
        presetGroup.presets.forEach((preset) => {
            const presetElement : HTMLOptionElement = document.createElement("option");
            presetElement.text = preset.name;
            presetElement.setAttribute('data-save', JSON.stringify(preset.shape));
            newPresetsElement.add(presetElement);
        });
    });
    const i = presetsElement.selectedIndex;
    presetsElement.innerHTML = newPresetsElement.innerHTML;
    presetsElement.selectedIndex = i;
};


export const saveCustomPreset = async (cells: Cells, presetName: string) => {
    const save = parseCellsToSave(cells);
    const presetsFile = await loadPresetsFile();
    let customPresets : PresetGroup = presetsFile.presetGroups[presetsFile.presetGroups.length-1];
    customPresets.presets.push({
        name: presetName,
        shape: save
    });
    await savePresetsFile(presetsFile);
};

const savePresetsFile = async (presetsFile: PresetsFile) => {
  localStorage.setItem('presets', JSON.stringify(presetsFile));
};



const parseCellsToSave = (cells: Cells) : PresetGrid => {
    let result: PresetGrid = [];
    cells.forEach((row, i) => {
       row.forEach((cell, j) => {
           if (cell.state == CellState.Alive) {
               result.push([i, j]);
           }
       });
    });
    return  result;
};

export const parseSaveToCells = (save: PresetGrid, cellAmount: number) => {
    const cells = createNewCells(cellAmount);
    save.forEach((coord) => {
        const cell = getCell(cells, coord[0], coord[1]);
        cell.state = CellState.Alive;
    });
    return cells;
};


const fetchDefaultPresets = async () : Promise<Object> => {
    const resp = await fetch("presets.json");
    return await resp.json();
};